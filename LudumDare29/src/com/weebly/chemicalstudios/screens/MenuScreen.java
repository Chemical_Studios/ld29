package com.weebly.chemicalstudios.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.weebly.chemicalstudios.ld29.LD29;

public class MenuScreen implements Screen {

	private BitmapFont font;
	private SpriteBatch batch;
	
	private Texture playBtnT, tutBtnT, quitBtnT;
	private Sprite playBtnS, tutBtnS, quitBtnS;
	
	private LD29 game;
	
	private Texture bg;
	private Sprite background;
	
	public MenuScreen(LD29 game) {
		this.game = game;
	}
	
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		batch.begin();
		batch.draw(background, 0, 0, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 1, 1, 0);
		
		font.setScale(1.0f);
		font.draw(batch, "Drilling Beneath the Surface", 30, 800);
		font.setScale(0.7f);
		font.draw(batch, "By: Chemical Studios", 110, 750);
		
		font.draw(batch, "Ludum Dare 29:", 150, 50);
		font.draw(batch, "Beneath the Surface", 125, 25);
		
		batch.draw(playBtnS, 35, 600, 0, 0, playBtnS.getWidth(), playBtnS.getHeight(), 1, 1, 0);
		
		batch.draw(tutBtnS, 35, 500, 0, 0, tutBtnS.getWidth(), tutBtnS.getHeight(), 1, 1, 0);
		
		batch.draw(quitBtnS, 35, 400, 0, 0, quitBtnS.getWidth(), quitBtnS.getHeight(), 1, 1, 0);
		
		batch.end();

		if(Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
			
			if(Gdx.input.getX() >= 38 && Gdx.input.getX() <= 412) {
				if(Gdx.input.getY() <= 360 && Gdx.input.getY() >= 300) { // PLAY
					game.setScreen(new PlayScreen(game));
				} else if(Gdx.input.getY() <= 460 && Gdx.input.getY() >= 400) { // HOW TO PLAY
					game.setScreen(new InstructionsScreen(game));
			} else if(Gdx.input.getY() <= 560 && Gdx.input.getY() >= 500) { // QUIT
					Gdx.app.exit();
				}
			}
		}
		
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		font = new BitmapFont(Gdx.files.internal("data/coolvetica.fnt"), Gdx.files.internal("data/coolvetica.png"), false);
		batch = new SpriteBatch();
		
		playBtnT = new Texture(Gdx.files.internal("img/buttons/play_btn.png"));
		playBtnS = new Sprite(playBtnT);
		
		tutBtnT = new Texture(Gdx.files.internal("img/buttons/how_to_btn.png"));
		tutBtnS = new Sprite(tutBtnT);
		
		quitBtnT = new Texture(Gdx.files.internal("img/buttons/quit_btn.png"));
		quitBtnS = new Sprite(quitBtnT);

		bg = new Texture(Gdx.files.internal("img/menu_background.png"));
		background = new Sprite(bg);
	}

	@Override
	public void hide() {
		this.dispose();
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		font.dispose();
		batch.dispose();
		playBtnT.dispose();
		quitBtnT.dispose();
	}
	
}
