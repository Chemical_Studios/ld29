package com.weebly.chemicalstudios.screens;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.weebly.chemicalstudios.TweenAccessors.SpriteTween;
import com.weebly.chemicalstudios.ld29.LD29;

public class SplashScreen implements Screen, KeyListener {

	Texture splashTexture;
	Sprite splashSprite;
	SpriteBatch batch;
	LD29 game;
	TweenManager manager;

	public SplashScreen(LD29 game) {
		this.game = game;
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		manager.update(delta);

		batch.begin();
		batch.draw(splashSprite, 0, 250, 0, 0, 900, 350, 1, 1, 0);
		batch.end();
		if (Gdx.input.isKeyPressed(Keys.ENTER)
				|| Gdx.input.isKeyPressed(Keys.SPACE)) {
			tweenCompleted();
		}
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void show() {
		splashTexture = new Texture("data/SplashScreen.png");
		splashTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		splashSprite = new Sprite(splashTexture);
		splashSprite.setColor(1, 1, 1, 0);

		batch = new SpriteBatch();

		Tween.registerAccessor(Sprite.class, new SpriteTween());

		manager = new TweenManager();

		TweenCallback callBack = new TweenCallback() {

			@Override
			public void onEvent(int type, BaseTween<?> source) {
				tweenCompleted();
			}

		};

		Tween.to(splashSprite, SpriteTween.ALPHA, 2.5f).target(1)
				.ease(TweenEquations.easeInQuad).repeatYoyo(1, 2.5f)
				.setCallback(callBack)
				.setCallbackTriggers(TweenCallback.COMPLETE).start(manager);

	}

	private void tweenCompleted() {
		game.setScreen(new MenuScreen(game));
	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		splashTexture.dispose();
		batch.dispose();
		game.dispose();
	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

}
