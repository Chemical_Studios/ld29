package com.weebly.chemicalstudios.tiles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.weebly.chemicalstudios.entities.Player;
import com.weebly.chemicalstudios.screens.PlayScreen;

public class DispenserTile extends Tile {
	public DispenserTile(int x, int y) {
		this.x = x;
		this.y = y;
		
		this.id = "dispenser";
		
		texture = new Texture(Gdx.files.internal("img/tiles/dispenser.png"));
		sprite = new Sprite(texture);
		visible = true;
	}
	
	public void update(Player player, PlayScreen play) {
		
		int preTouch = player.getDepGem();
		
		if(player.getX() + player.getSprite().getWidth() >= getX() && player.getX() <= getX() + getWidth()) {
			if(player.getY() >= getY() && player.getY() <= getY() + getHeight()) {
				for(int i = 0; i < player.getInvGem(); i++) {
					player.setInvGem(player.getInvGem()-1);
					player.setDepGem(player.getDepGem()+1);
				}

				if(player.getDepGem() > preTouch) {
					play.regenWorld();
					if(player.getLives() < 3) {
						player.setLives(player.getLives() + 1);
					}
				}
			}
		}
	}
	
	public Sprite getSprite() {
		return sprite;
	}
}
