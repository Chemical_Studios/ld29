package com.weebly.chemicalstudios.tiles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Tile {
	protected int x, y;
	protected Texture texture;
	protected Sprite sprite;
	protected boolean destroyed = false;
	protected boolean visible;
	protected boolean isGem = false;
	protected String id;
	protected SpriteBatch batch;

	public void setDestroyed(boolean destroyed) {
		this.destroyed = destroyed;

	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isDestroyed() {
		return destroyed;
	}

	public boolean isVisible() {
		return visible;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getWidth() {
		return 64;
	}

	public int getHeight() {
		return 64;
	}

	public Sprite getSprite() {
		return sprite;
	}

	public void dispose() {
		texture.dispose();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void sparkle(SpriteBatch batch) {
	}
}
