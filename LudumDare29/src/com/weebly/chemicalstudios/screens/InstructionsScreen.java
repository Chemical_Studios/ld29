package com.weebly.chemicalstudios.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.weebly.chemicalstudios.ld29.LD29;

public class InstructionsScreen implements Screen {

	LD29 game;
	private SpriteBatch batch;
	private Texture instrT, menu_btnT;
	private Sprite instrS, menu_btnS;
	
	private Texture dep_boxT, trapT;
	private Sprite dep_box, trap;
	
	private TextureAtlas atlas;
	private Animation animation;
	private float elapsedTime = 0;
	
	public InstructionsScreen(LD29 game) {
		this.game = game;
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		batch.begin();
		
		instrS.draw(batch);
		
		batch.draw(menu_btnS, 25, 20, 0, 0, menu_btnS.getWidth(), menu_btnS.getHeight(), 1, 1, 0);
		
		batch.draw(dep_box, 60, 250, 0, 0, 64, 64, 1, 1, 0);
		
		batch.draw(trap, 315, 420, 0, 0, 64, 64, 1, 1, 0);
		
		elapsedTime += Gdx.graphics.getDeltaTime();
		batch.draw(animation.getKeyFrame(elapsedTime, true), 60, 420, 0, 0, 64, 64, 1, 1, 0);
		
		batch.end();
		
		if(Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
			if(Gdx.input.getX() >= 25 && Gdx.input.getX() <= 405) {
				if(Gdx.input.getY() >= 880 && Gdx.input.getY() <= 940) {
					game.setScreen(new MenuScreen(game));
				}
			}
		}
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		batch = new SpriteBatch();
		instrT = new Texture(Gdx.files.internal("img/instr.png"));
		instrS = new Sprite(instrT);
		
		menu_btnT = new Texture(Gdx.files.internal("img/buttons/menu_btn.png"));
		menu_btnS = new Sprite(menu_btnT);
		
		dep_boxT = new Texture(Gdx.files.internal("img/tiles/dispenser.png"));
		dep_box = new Sprite(dep_boxT);
		
		trapT = new Texture(Gdx.files.internal("img/tiles/stone_trap.png"));
		trap = new Sprite(trapT);
		
		atlas = new TextureAtlas(Gdx.files.internal("img/tiles/gem/gem.pack"));
		animation = new Animation(1/15f, atlas.getRegions());
	}

	@Override
	public void hide() {
		this.dispose();
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		batch.dispose();
		menu_btnT.dispose();
		instrT.dispose();
	}

}
