package com.weebly.chemicalstudios.screens;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.weebly.chemicalstudios.entities.Player;
import com.weebly.chemicalstudios.ld29.LD29;
import com.weebly.chemicalstudios.tiles.DirtTile;
import com.weebly.chemicalstudios.tiles.DispenserTile;
import com.weebly.chemicalstudios.tiles.GemTile;
import com.weebly.chemicalstudios.tiles.GrassTile;
import com.weebly.chemicalstudios.tiles.HiddenTile;
import com.weebly.chemicalstudios.tiles.SignTile;
import com.weebly.chemicalstudios.tiles.StoneTile;
import com.weebly.chemicalstudios.tiles.Tile;
import com.weebly.chemicalstudios.tiles.TrapTile;

public class PlayScreen implements Screen {

	OrthographicCamera camera;
	SpriteBatch batch;
	
	private Random rng;
	
	private Player player;
	
	private boolean gemPlaced = false;
	
	Tile[][] tiles;
	HiddenTile placeHolder;
	DispenserTile dispenser;
	SignTile sign;
	
	private int tileWidth = 7, tileHeight = 9;
	
	private BitmapFont font;
	
	private Texture backgroundTexture;
	private Sprite background;
	
	private Texture gemTexture;
	private Sprite gem_icon;
	
	private int winAmount = 10;
	
	public Music bgMusic;
	
	public boolean win = false;
	
	/* gem animation */
	private TextureAtlas atlas;
	private Animation animation;
	private float elapsedTime = 0;
	
	private LD29 game;
	
	public PlayScreen(LD29 game) {
		this.game = game;
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		
		/* Begin */
		batch.begin();
		
		// Draw background
		batch.draw(background, 0, 0, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 1, 1, 0);
		
		// Draw all tiles
		for(int i = 0; i < tileHeight; i++) {
			for(int j = 0; j < tileWidth; j++) {
				if(!tiles[j][i].isDestroyed()) {
					if(tiles[j][i].isVisible()) {
						//Draw the gem's animation
						if(tiles[j][i].getId().equals("gem")) {
							elapsedTime += Gdx.graphics.getDeltaTime();
							batch.draw(animation.getKeyFrame(elapsedTime, true), tiles[j][i].getX(), tiles[j][i].getY(), 0, 0, 64, 64, 1, 1, 0);
						} else {
							batch.draw(tiles[j][i].getSprite(), tiles[j][i].getX(), tiles[j][i].getY(), 0, 0, tiles[j][i].getWidth(), tiles[j][i].getHeight(), 1, 1, 0);						
						}
					// if we haven't discovered the tile, just use the "placeholder" tile as a "fog of war" effect
					} else {
						batch.draw(placeHolder.getSprite(), tiles[j][i].getX(), tiles[j][i].getY(), 0, 0, tiles[j][i].getWidth(), tiles[j][i].getHeight(), 1, 1, 0);
					}
				}
			}
		}
		// Dispenser and sign
		batch.draw(dispenser.getSprite(), dispenser.getX(), dispenser.getY(), 0, 0, dispenser.getWidth(), dispenser.getHeight(), 1, 1, 0);
		batch.draw(sign.getSprite(), sign.getX(), sign.getY(), 0, 0, sign.getWidth(), sign.getHeight(), 1, 1, 0);
		
		// Draw player
		batch.draw(player.getSprite(), player.getX(), player.getY(), 0, 0, 64, 64, 1, 1, 0);
		
		// 0/1 gems
		font.draw(batch, player.getInvGem() + "/1 gems", 325, 800); // GemInv
		
		// [Gem] 0/10
		batch.draw(gem_icon, 330, 720, 0, 0, gem_icon.getWidth()*3, gem_icon.getHeight()*3, 1, 1, 0); // Gem icon to the left of "GemDep"
		font.draw(batch, player.getDepGem() + "/" + winAmount, 380, 750); // GemDep
		
		// Lives
		font.draw(batch, player.getLives() + " lives", 10, 950);
		
		// Score
		font.draw(batch, String.valueOf(player.getScore()), 350, 950);
		
		
		if(player.getInvGem() > 0) {
			font.draw(batch, "Deposit Gem! ->", 150, 620);
		}
		
		
		player.update(delta, tiles);
		dispenser.update(player, this);
		
		/* Check for game over */
		if(player.getLives() <= 0) { // Died
			resetGame();
		}
		
		if(player.getDepGem() >= 10) { // Won game
			win = true;
			font.draw(batch, "You won with a score of " + player.getScore() + "!", 20, 500);
			font.draw(batch, "Press spacebar to play again", 20, 450);
			font.draw(batch, "or press Escape to go", 50, 400);
			font.draw(batch, "to the main menu!", 80, 350);
			if(Gdx.input.isKeyPressed(Keys.SPACE)) {
				resetGame();
			}
		}
		
		if(Gdx.input.isKeyPressed(Keys.ESCAPE)) {
			game.setScreen(new MenuScreen(game));
		}
		
		
		batch.end();
		/* End */
		
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {
		/* Initialize variables here */
		camera = new OrthographicCamera();
		batch = new SpriteBatch();
		
		rng = new Random();
		
		player = new Player(190, (64 * 11), this);
		Gdx.input.setInputProcessor(player);
		
		tiles = new Tile[tileWidth][tileHeight];
		regenWorld();
		
		placeHolder = new HiddenTile(0,0);

		dispenser = new DispenserTile(6*64, 9*64);
		sign = new SignTile(6*64, 10*64);
		
		font = new BitmapFont(Gdx.files.internal("data/coolvetica.fnt"), Gdx.files.internal("data/coolvetica.png"), false);
		
		backgroundTexture = new Texture(Gdx.files.internal("img/background.png"));
		background = new Sprite(backgroundTexture);
		
		gemTexture = new Texture(Gdx.files.internal("img/gem_icon.png"));
		gem_icon = new Sprite(gemTexture);
		
		bgMusic = Gdx.audio.newMusic(Gdx.files.internal("data/song.mp3"));
		bgMusic.setVolume(0.05f);
		bgMusic.setLooping(true);
		bgMusic.play();
		
		atlas = new TextureAtlas(Gdx.files.internal("img/tiles/gem/gem.pack"));
		animation = new Animation(1/15f, atlas.getRegions());
	}

	@Override
	public void hide() {
		this.dispose();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		batch.dispose();
		player.dispose();
		dispenser.dispose();
		gemTexture.dispose();
		font.dispose();
		sign.dispose();
		atlas.dispose();
	}
	
	public void regenWorld() {
		int chance = rng.nextInt(15);
		int t_chance = rng.nextInt(10);
		
		gemPlaced = false;
		
		for(int i = 0; i < tileHeight; i++) {
			for(int j = 0; j < tileWidth; j++) {
				
				if(i == 8) {
					tiles[j][i] = new GrassTile(j*64, i*64);
				} else if(i == 7 || i == 6) {
					tiles[j][i] = new DirtTile(j*64, i*64 );
				} else if(i <= 5) {
					tiles[j][i] = new StoneTile(j*64, i*64);
					
					t_chance = rng.nextInt(10);
					chance = rng.nextInt(15);

					if(t_chance == 3) {
						tiles[j][i] = new TrapTile(j*64, i*64);
					}
					
					if(!gemPlaced) {
						if(chance == 1) {
							tiles[j][i] = new GemTile(j*64, i*64);
							gemPlaced = true;
						}
					}
				}
			}
		}
		
		// If a gem tile wasn't placed, regenerate the world again until one is finally placed 
		if(!gemPlaced)
			regenWorld();
	}
	
	public void resetGame() {
		player.setLives(3);
		
		player.setScore(0);
		
		player.setInvGem(0);
		player.setDepGem(0);
		
		win = false;
		
		regenWorld();
		player.setPosition(205, (64*11));
	}
	
}
