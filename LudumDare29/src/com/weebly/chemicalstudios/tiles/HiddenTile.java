package com.weebly.chemicalstudios.tiles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class HiddenTile extends Tile {
	public HiddenTile(int x, int y) {
		this.x = x;
		this.y = y;
		
		texture = new Texture(Gdx.files.internal("img/tiles/hidden.png"));
		sprite = new Sprite(texture);
		visible = true;
	}
}
