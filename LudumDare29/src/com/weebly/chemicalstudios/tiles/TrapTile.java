package com.weebly.chemicalstudios.tiles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class TrapTile extends Tile {
	public TrapTile(int x, int y) {
		this.x = x;
		this.y = y;
		
		this.id = "pre_trap";
		
		texture = new Texture(Gdx.files.internal("img/tiles/stone_trap.png"));
		sprite = new Sprite(texture);
	}
}
