package com.weebly.chemicalstudios.ld29;

import com.badlogic.gdx.Game;
import com.weebly.chemicalstudios.screens.MenuScreen;

public class LD29 extends Game {
	
	/*
	 * Compo entry for Ludum Dare 28 by Chemical Studios
	 * 4/25/2014- 4/27/2014
	 * 
	 * ..sorry for the code, I was in a hurry :P
	 */
	
	
	@Override
	public void create() {
		setScreen(new MenuScreen(this));
	}
	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
	public void render() {
		super.render();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}
}
