package com.weebly.chemicalstudios.entities;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.weebly.chemicalstudios.screens.PlayScreen;
import com.weebly.chemicalstudios.tiles.Tile;

public class Player implements InputProcessor {

	private int x, y;
	private Texture playerTexture_left;
	private Texture playerTexture_right;
	private Texture playerTexture_up;
	private Texture playerTexture_down;

	private Sprite playerSprite_left;
	private Sprite playerSprite_right;
	private Sprite playerSprite_up;
	private Sprite playerSprite_down;

	private Sprite playerSprite;

	private String direction = "right";
	private String left = "left", right = "right", up = "up", down = "down";

	private int tileWidth = 7, tileHeight = 9;
	
	private int gemInv; // gems currently holding
	private int gemDep = 0; // gems already deposited
	
	private int lives = 3;
	
	private int score = 0;
	
	PlayScreen game;
	
	Sound miningSound;
	Sound gemSound;
	Sound hit;
	
	public Player(int x, int y, PlayScreen game) {
		this.x = x;
		this.y = y;
		this.game = game;

		playerTexture_right = new Texture(
				Gdx.files.internal("img/Player_right.png"));
		playerSprite_right = new Sprite(playerTexture_right);

		playerTexture_left = new Texture(
				Gdx.files.internal("img/Player_left.png"));
		playerSprite_left = new Sprite(playerTexture_left);

		playerTexture_down = new Texture(
				Gdx.files.internal("img/Player_down.png"));
		playerSprite_down = new Sprite(playerTexture_down);

		playerTexture_up = new Texture(Gdx.files.internal("img/Player_up.png"));
		playerSprite_up = new Sprite(playerTexture_up);

		playerSprite = playerSprite_right;

		playerSprite.setSize(63, 63);
		
		miningSound = Gdx.audio.newSound(Gdx.files.internal("data/dtt.mp3"));
		gemSound = Gdx.audio.newSound(Gdx.files.internal("data/gem.wav"));
		hit = Gdx.audio.newSound(Gdx.files.internal("data/hit.mp3"));
	}

	public void update(float delta, Tile[][] tiles) {

		if(x <= 0) {
			x = 0;
		} else if(x + getSprite().getWidth() >= Gdx.graphics.getWidth()) {
			x = (int) (Gdx.graphics.getWidth() - getSprite().getWidth());
		}
		if(y <= 0){
			y = 0;
		} else if(y >= 580) {
			y = 579;
		}
		
		for(int i = 0; i < tileHeight; i++) {
			for(int j = 0; j < tileWidth; j++) {
				
				if(x + getSprite().getWidth() >= tiles[j][i].getX() && x+5 <= tiles[j][i].getX() + tiles[j][i].getWidth()) {
					if(y >= tiles[j][i].getY() && y-5 <= tiles[j][i].getY()-5 + tiles[j][i].getHeight()) {
						
						/* Score */
						if(!tiles[j][i].isDestroyed()) {
							miningSound.play(0.05f);
							if(tiles[j][i].getId().equals("grass")) {
								score += 5;
							} else if(tiles[j][i].getId().equals("dirt")) {
								score += 10;
							} else if(tiles[j][i].getId().equals("stone")) {
								score += 15;
							} else if(tiles[j][i].getId().equals("gem")) {
								gemSound.play(0.5f);
								score += 100;
							} else if(tiles[j][i].getId().equals("pre_trap")) {
								score -= 15;
								hit.play(0.5f);
							}
						}
						
						// if collide, destroy tile, and set adjacent tiles to be visible
						tiles[j][i].setDestroyed(true);
						
						if(j < tileWidth-1) {
							tiles[j+1][i].setVisible(true);
						}
						if(j > 0) {
							tiles[j-1][i].setVisible(true);
						}
						
						if(i > 0) {
							tiles[j][i-1].setVisible(true);
						}
						if(i < tileHeight-1) {
							tiles[j][i+1].setVisible(true);
						}
						
						// Add gem to count
						if(tiles[j][i].getId().equals("gem")) {
							if(gemInv < 1) {
								gemInv++;
							}
						// Take off a life, and change the ID so the trap doesn't hurt you while standing in its old place
						} else if(tiles[j][i].getId().equals("pre_trap")) { 
							if(lives >= 1) lives--;
							tiles[j][i].setId("post_trap");
						}
					}
				}
			}
		}
		
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Sprite getSprite() {
		if (direction.equals(left)) {
			playerSprite = playerSprite_left;
		} else if (direction.equals(right)) {
			playerSprite = playerSprite_right;
		} else if (direction.equals(up)) {
			playerSprite = playerSprite_up;
		} else if (direction.equals(down)) {
			playerSprite = playerSprite_down;
		}
		return playerSprite;
	}

	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
	public boolean keyDown(int keycode) {
		if(!game.win) {
			if (keycode == (Keys.A) || keycode == (Keys.LEFT)) {
				x -= 64;
				direction = left;
			}
			if (keycode == (Keys.D) || keycode == (Keys.RIGHT)) {
				x += 64;
				direction = right;
			} 
			if (keycode == (Keys.W) || keycode == (Keys.UP)) {
				y += 64;
				direction = up;
			}
			if (keycode == (Keys.S) || keycode == (Keys.DOWN)) {
				y -= 64;
				direction = down;

			}
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// Mute music
		if(keycode == Keys.M) {
			if(game.bgMusic.isPlaying()) {
				game.bgMusic.stop();
			} else {
				game.bgMusic.play();
			}
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
	
	public void dispose() {
		playerTexture_left.dispose();
		playerTexture_right.dispose();
		playerTexture_up.dispose();
		playerTexture_down.dispose();
		miningSound.dispose();
	}
	
	public int getInvGem() {
		return gemInv;
	}

	public int getDepGem() {
		return gemDep;
	}

	public void setInvGem(int i) {
		this.gemInv = i;
	}
	public void setDepGem(int i) {
		this.gemDep = i;
	}
	
	public int getLives() {
		return lives;
	}
	
	public void setLives(int lives) {
		this.lives = lives;
	}
	
	public int getScore() {
		return score;
	}
	
	public void setScore(int score) {
		this.score = score;
	}
}
