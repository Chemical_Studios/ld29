package com.weebly.chemicalstudios.tiles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class GrassTile extends Tile {
	public GrassTile(int x, int y) {
		this.x = x;
		this.y = y;
		
		this.id = "grass";
		
		texture = new Texture(Gdx.files.internal("img/tiles/grass.png"));
		sprite = new Sprite(texture);
		visible = true;
	}
}
