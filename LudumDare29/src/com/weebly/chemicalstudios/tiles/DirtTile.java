package com.weebly.chemicalstudios.tiles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class DirtTile extends Tile {
	public DirtTile(int x, int y) {
		this.x = x;
		this.y = y;
		
		this.id = "dirt";
		
		texture = new Texture(Gdx.files.internal("img/tiles/dirt.png"));
		sprite = new Sprite(texture);
	}
}
