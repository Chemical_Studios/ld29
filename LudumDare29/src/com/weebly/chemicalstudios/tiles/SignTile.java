package com.weebly.chemicalstudios.tiles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class SignTile extends Tile {
	public SignTile(int x, int y) {
		this.x = x;
		this.y = y;
		
		this.id = "sign";
		
		texture = new Texture(Gdx.files.internal("img/tiles/sign.png"));
		sprite = new Sprite(texture);
		visible = true;
	}
}
