package com.weebly.chemicalstudios.ld29;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.weebly.chemicalstudios.ld29.LD29;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Ludum Dare 29: Beneath the Surface";
		cfg.useGL20 = true;
		cfg.width = 445;
		cfg.height = 960;
		
		new LwjglApplication(new LD29(), cfg);
	}
}
