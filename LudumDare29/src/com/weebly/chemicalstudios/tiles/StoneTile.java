package com.weebly.chemicalstudios.tiles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class StoneTile extends Tile {
	public StoneTile(int x, int y) {
		this.x = x;
		this.y = y;
		
		this.id = "stone";
		
		texture = new Texture(Gdx.files.internal("img/tiles/stone.png"));
		sprite = new Sprite(texture);
	}
}
