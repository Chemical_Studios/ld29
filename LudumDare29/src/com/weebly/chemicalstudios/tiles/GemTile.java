package com.weebly.chemicalstudios.tiles;

public class GemTile extends Tile {
	public GemTile(int x, int y) {
		this.x = x;
		this.y = y;
		this.id = "gem";
	}
}
